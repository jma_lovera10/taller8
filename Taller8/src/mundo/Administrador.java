package mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Nodo;

/**
 * Clase que representa el administrador del sistema integrado de transporte
 * @author SamuelSalazar
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";
	
	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";

	
	/**
	 * Grafo que modela el sistema integrado de transporte
	 */
	private GrafoNoDirigido<Estacion> grafo;

	
	/**
	 * Construye un nuevo administrador del SITP
	 */
	public Administrador() 
	{
		grafo = new GrafoNoDirigido<Estacion>();
	}

	/**
	 * Devuelve todas las rutas que pasan por un paadero con nombre dado
	 * @param nombre 
	 * @return Arreglo con los nombres de las rutas que pasan por el estacion requerido
	 */
	public String[] darRutasParadero(String nombre)
	{
		//TODO Implementar
		return null;
	}

	/**
	 * Devuelve la distancia que hay entre los paraderos mas cercanos del sistema.
	 * @return distancia minima entre 2 paraderos
	 */
	public double distanciaMinimaParaderos()
	{
		//TODO Implementar
		return -1;
	}
	
	
	public Estacion darParadero(int id)
	{
		//TODO Implementar
		return null;
	}
	/**
	 * Devuelve la distancia que hay entre los paraderos más lejanos del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaParaderos()
	{
		//TODO Implementar
		return -1;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar
		BufferedReader bf = new BufferedReader(new FileReader(new File(RUTA_PARADEROS)));
		int tam = Integer.parseInt(bf.readLine());
		String[] split;
		int i = 0;
		while(i<tam){
			split = bf.readLine().split(";");
			grafo.agregarNodo(new Estacion(split[0].hashCode(), Double.parseDouble(split[1]), Double.parseDouble(split[2]), split[0]));
			i++;
		}
		bf.close();
		System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" Estaciones");
		
		//TODO Implementar
		bf = new BufferedReader(new FileReader(new File(RUTA_RUTAS)));
		tam = Integer.parseInt(bf.readLine());
		i = 0;
		int j = 0;
		int distance;
		String routeName;
		String station;
		while(i++<tam){
			bf.readLine();
			routeName = bf.readLine();
			j = Integer.parseInt(bf.readLine());
			if(j==0)continue;
			split = bf.readLine().split(" ");
			distance = Integer.parseInt(split[1]);
			station = split[0];
			while(j-->1){
				split = bf.readLine().split(" ");
				grafo.agregarArco(station.hashCode(), split[0].hashCode(), distance, routeName);
				distance = Integer.parseInt(split[1]);
				station = split[0];
			}
		}
		bf.close();
		System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" arcos");
	}

	/**
	 * Busca el estacion con nombre dado<br>
	 * @param nombre el nombre del estacion buscado
	 * @return estacion cuyo nombre coincide con el parametro, null de lo contrario
	 */
	public Estacion buscarParadero(String nombre)
	{
		//TODO Implementar
		return null;
	}

}
