package estructuras;

public class TablaHash<K extends Comparable<K> ,V> {

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	private NodoHash<K, V>[] tabla;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		this(500000,5);
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) {
		this.factorCargaMax = factorCargaMax;
		this.capacidad = capacidad;
		count = 0;
		factorCarga = 0;
		tabla = new NodoHash[capacidad];
	}

	public boolean put(K llave, V valor){
		if(factorCarga>=factorCargaMax)return false;
		
		int pos = hash(llave);
		NodoHash<K, V> nodo = tabla[pos];
		if(nodo==null) tabla[pos] = new NodoHash<K, V>(llave, valor);
		else{
			if(nodo.getLlave().compareTo(llave)==0){
				nodo.setValor(valor);
				return true;
			}
			while(nodo.getNext()!=null){
				if(nodo.getNext().getLlave().compareTo(llave)==0){
					nodo.getNext().setValor(valor);
					return true;
				}
				nodo = nodo.getNext();
			}
			nodo.setNext(new NodoHash<K, V>(llave, valor));
		}
		count++;
		actualizarFactor();
		return true;
	}

	public V get(K llave){
		int pos = hash(llave);
		V elem = null;
		NodoHash<K, V> actual = tabla[pos];
		while(elem==null){
			if(actual==null)break;
			else if(actual.getLlave().compareTo(llave)==0)elem = actual.getValor();
			else actual = actual.getNext();
		}

		return elem;
	}

	public V delete(K llave){
		int pos = hash(llave);
		V elem = null;
		NodoHash<K, V> actual = tabla[pos];
		if(actual == null) return elem;
		else if(actual.getLlave().compareTo(llave)==0){
			elem =actual.getValor();
			tabla[pos] = tabla[pos].getNext();
		}
		else{
			while(actual.getNext()!=null){
				if(actual.getNext().getLlave().compareTo(llave)==0){
					elem = actual.getNext().getValor();
					actual.setNext(actual.getNext().getNext());
					count--;
					actualizarFactor();
					break;
				}
				actual = actual.getNext();
			}
		}

		return elem;
	}

	private void actualizarFactor() {
		factorCarga = count/capacidad;
	}
	
	
	public int size(){
		return count;
	}
	//Hash
	private int hash(K llave)
	{
		return llave.hashCode()%capacidad;
	}

}