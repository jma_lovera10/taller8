package modelos;

import estructuras.Nodo;

/**
 * Clase que representa un paradero del sistema integrado de transporte SITP
 * @author SamuelSalazar
 */
//TODO La clase debe implementar la interface Nodo
public class Estacion implements Nodo{

	/**
	 * Latitud del estación
	 */
	private double latitud;
	
	/**
	 * Longitud del estación
	 */
	private double longitud;
	
	/**
	 * Nombre del paradero
	 */
	private String nombre;
	
	/**
	 * Identificador único del paradero
	 */
	private int id;
	
	
	/**
	 * Construye un nuevo paradero con un nombre e id dado en una ubicacion
	 * representada por la latitud y la longitud.
	 * @param id Indentificador unico del paradero
	 * @param latitud 
	 * @param longitud 
	 * @param nombre
	 */
	public Estacion(int id, double latitud, double longitud, String nombre) {
		this.id = id;
		this.latitud = latitud;
		this.longitud = longitud;
		this.nombre = nombre;
	}
	
	public int darId(){
		return id;
	}
	
	/**
	 * Devuelve la latitud del paradero
	 * @return latitud
	 */
	public double darLatitud() {
		return latitud;
	}
	
	/**
	 * Devuelve la longitud del paradero
	 * @return longitud
	 */
	public double darLongitud() {
		return longitud;
	}
	
	/**
	 * Devuelve el nombre del paradero
	 * @return nombre
	 */
	public String darNombre() {
		return nombre;
	}
	
	/**
	 * nombre (latitud, longitud)
	 */
	@Override
	public String toString() {
		return nombre+" ("+latitud+" , "+longitud+")";
	}

	@Override
	public int compareTo(Nodo o) 
	{
		return o.darId() - id;
	}
	
	public int hashCode(){
		return id;
	}
}
